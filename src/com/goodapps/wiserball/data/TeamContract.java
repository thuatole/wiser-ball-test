package com.goodapps.wiserball.data;

import android.provider.BaseColumns;

public class TeamContract {
	public TeamContract() {
	}

	public static abstract class TeamEntry implements BaseColumns {
		public static final String TABLE_NAME = "team";
		public static final String COLUMN_TEAM_NAME = "teamname";
	}

	private static final String TEXT_TYPE = " TEXT";

	public static final String SQL_CREATE_ENTRIES = "CREATE TABLE " + TeamEntry.TABLE_NAME + " ("
			+ TeamEntry._ID + " INTEGER PRIMARY KEY," + TeamEntry.COLUMN_TEAM_NAME + TEXT_TYPE
			+ " )";

	public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + TeamEntry.TABLE_NAME;
	
	public long id;
	public String name;	
	
	@Override
	public String toString() {
		return name;
	}
}
