package com.goodapps.wiserball.type;

public enum GameState {
	Closed,
	Setupping,
	Playing,
	Opening,
}
